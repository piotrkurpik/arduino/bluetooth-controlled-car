#include <SPI.h>

#include "MotorDriver.hpp"

DualMotorDriver Driver(5, 6, 7, 8, 9, 10, 11);

int speed = 0;
int speed_step = 10;
double direction = 1.0;
double direction_step = 1.05;
int key = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(12, OUTPUT);
	digitalWrite(12, HIGH);
	Driver.setup();
	Driver.enable();
}
void loop()
{
  if(Serial.available() > 0)
  {
    key = Serial.read();
		switch(key) {
		case '1': // up
			speed += speed_step;
			break;
		case '2': // down
			speed -= speed_step;
			break;
		case '3': // right
			direction *= direction_step;
			break;
		case '4': // left
			direction /= direction_step;
			break;
		case '5': // middle
			direction = 1.0;
			break;
		case '6': // break
			speed = 0;
			break;
			
		}
		Driver.A.rotate(speed*direction); // left wheel
		Driver.B.rotate(speed/direction); // right wheel
  }
}
