#include <SPI.h>

#include "MotorDriver.hpp"

DualMotorDriver::DualMotorDriver(int PWMA, int AIN2, int AIN1, int STBY,
				 int BIN1, int BIN2, int PWMB) {
  this->A = MotorDriver(AIN1, AIN2, PWMA);
  this->B = MotorDriver(BIN1, BIN2, PWMB);
  this->STBY = STBY;
}

void DualMotorDriver::setup() {
  pinMode(STBY, OUTPUT);
  A.setup();
  B.setup();
}

void DualMotorDriver::enable() {
  digitalWrite(STBY, HIGH);
}

void DualMotorDriver::disable() {
  digitalWrite(STBY, LOW);
}

MotorDriver::MotorDriver(int IN1, int IN2, int PWM) {
  this->IN1 = IN1;
  this->IN2 = IN2;
  this->PWM = PWM;
}

MotorDriver::MotorDriver() {
  this->IN1 = 0;
  this->IN2 = 0;
  this->PWM = 0;
}

void MotorDriver::setup() {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(PWM, OUTPUT);
}

void MotorDriver::rotate(int value) {
  if(value>0 and value < 256) {
    this->rotateForward(value);
  } else if(value>=256) {
    this->rotateForward(255);
  } else if(value==0) {
    this->hardBrake();
  } else if(value<0 and value>-256) {
    this->rotateBackward(-1 * value);
  } else if(value<=-256) {
    this->rotateBackward(255);
  }
}

void MotorDriver::rotateForward(int value) {
  analogWrite(PWM, value);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
}

void MotorDriver::rotateBackward(int value) {
  analogWrite(PWM, value);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
}

void MotorDriver::softBrake() {
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}

void MotorDriver::hardBrake() {
  analogWrite(PWM, 0);
}
