#ifndef MOTOR_DRIVER
#define MOTOR_DRIVER

class MotorDriver
{
private:
  int IN1;
  int IN2;
  int PWM;
  
  
public:
  MotorDriver(int IN1, int IN2, int PWM);
  MotorDriver();

  void setup();
  void rotate(int value);
  void rotateForward(int value);
  void rotateBackward(int value);
  void softBrake();
  void hardBrake();
  
};

class DualMotorDriver
{
private:
  int STBY;

public:
  MotorDriver A;
  MotorDriver B;
  
  DualMotorDriver(int PWMA, int AIN2, int AIN1, int STBY,
		  int BIN1, int BIN2, int PWMB);
  void setup();
  void enable();
  void disable();
};

#endif
